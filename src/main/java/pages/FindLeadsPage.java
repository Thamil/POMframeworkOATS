package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{
	
	public FindLeadsPage clickPhoneMenu()
	{
		WebElement menuPhone = locateElement("xpath", "(//span[@class='x-tab-strip-inner'])[2]/span");
		click(menuPhone);
		return this;
	}
	
	public FindLeadsPage typePhoneNumber(String data)
	{
		WebElement textPhoneNumber = locateElement("xpath", "//input[@name='phoneNumber']");
		type(textPhoneNumber, data);
		return this;
	}
	
	public FindLeadsPage clickFindLeadsButton() throws InterruptedException
	{
		WebElement buttonFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(buttonFindLeads);
		Thread.sleep(5000);
		return this;
	}
	
	public ViewLeadPage clickLead() throws InterruptedException
	{
		WebElement linkLead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(linkLead);
		Thread.sleep(5000);
		return new ViewLeadPage();
	}

}