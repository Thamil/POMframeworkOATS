package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{
	
	public EditLeadPage typeCompanyName(String data)
	{
		WebElement textCompanyName = locateElement("xpath", "(//input[@name='companyName'])[2]");
		type(textCompanyName, data);
		return this;
	}
	
	public EditLeadPage clickUpdateButton()
	{
		WebElement buttonUpdate = locateElement("xpath", "//input[@class='smallSubmit']");
		click(buttonUpdate);
		return this;
		
	}

}
