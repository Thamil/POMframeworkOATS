package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{
	
	public EditLeadPage clickEdit()
	{
		WebElement buttonEdit = locateElement("linktext", "Edit");
		click(buttonEdit);
		return new EditLeadPage();
	}

}
